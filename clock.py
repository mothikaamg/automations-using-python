from tkinter import *
from time import strftime

clock = Tk()
clock.title("My Digital clock")

lbl = Label(clock,font="arial 160 bold",bg="black",fg="White")
lbl.pack(anchor="center",fill="both",expand ="yes")

def time():
    string = strftime(" %H:%M:%S %p ")
    lbl.config(text = string)
    lbl.after(1000,time)

time()
mainloop()