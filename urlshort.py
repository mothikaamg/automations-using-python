import pyshorteners


print("Welcome to URL Shortener !!")
url = input("Enter the URL to convert : ")

short_url = pyshorteners.Shortener().tinyurl.short(url)

print("short URL : ", short_url)