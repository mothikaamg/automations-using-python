import pyqrcode
import png

print("Welcome to QR Code Generator")

msg = input("Type your secret message : ")

code = pyqrcode.create(msg)
code.png("QRcode.png",scale=5)

print("QR Code Generated successfully!!!")