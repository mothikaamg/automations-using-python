from pyzbar.pyzbar import decode
from PIL import Image


print("Welcome to QR code Reader !!!")

img = Image.open("QRcode.png")

## Decoding the Image

d = decode(img)
print(d[0].data.decode())
